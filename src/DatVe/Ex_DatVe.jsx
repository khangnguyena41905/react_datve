import React, { Component } from "react";
import "../asset/CSS/BaiTapBookingTicket.css";
import FormInfor from "./Component/FormInfor";
import Selection from "./Component/Selection";
import Table from "./Component/Table";

export default class Ex_DatVe extends Component {
  render() {
    return (
      <div className="background py-32">
        <div className="container-content max-w-none md:max-w-4xl mx-auto p-16">
          <FormInfor />
          <div className="Selection my-32">
            <Selection />
          </div>
          <Table />
        </div>
      </div>
    );
  }
}
