import React, { Component } from "react";
import { connect } from "react-redux";
import "../../asset/CSS/BaiTapBookingTicket.css";
import { guiGheDaChon } from "../../redux/action/action";

class Selection extends Component {
  state = {
    seatsAvailable: [...this.props.seatsAvailable],
    gheDangChon: [],
  };

  timViTriGheDangChon = (gheDangChon, soGhe) => {
    let index = gheDangChon.indexOf(soGhe);
    return index;
  };

  renderSeat = (soGhe, daDat, isDisable, onChangFuntion) => {
    // hàm này để tạo ra cái ghế
    return (
      <td>
        <input
          onChange={onChangFuntion}
          name={soGhe}
          value={daDat}
          type="checkbox"
          disabled={isDisable}
        />
      </td>
    );
  };
  handleSelectSeats = (e) => {
    let checkbox = e.target;
    let gheDuocChon = checkbox.name;
    let gheDangChonClone = [...this.state.gheDangChon];

    if (checkbox.checked) {
      gheDangChonClone.push(gheDuocChon);
    } else {
      let index = gheDangChonClone.indexOf(checkbox.name);
      gheDangChonClone.splice(index, 1);
    }

    this.setState({
      gheDangChon: gheDangChonClone,
    });
  };
  renderNumberRow = (row) => {
    let { hang, danhSachGhe } = row;
    let contentHTML = [];
    for (let index = 0; index < danhSachGhe.length; index++) {
      if (index == 0) {
        contentHTML.push(<td className="firstChar">{hang}</td>);
      }
      if (index == 6) {
        contentHTML.push(<td></td>);
      }
      contentHTML.push(<td>{danhSachGhe[index].soGhe}</td>);
    }
    return <tr>{contentHTML}</tr>;
  };
  renderRow = (row, customerInfor) => {
    let { gheDangChon } = this.state;
    let { hang, danhSachGhe } = row;
    let contentHTML = [];
    // duyệt từng phần tử trong hàng
    for (let index = 0; index < danhSachGhe.length; index++) {
      let { soGhe, daDat, gia } = danhSachGhe[index];
      let viTriGheDaChon = this.timViTriGheDangChon(gheDangChon, soGhe);
      // render số hàng
      if (index == 0) {
        contentHTML.push(<td className="firstChar">{hang}</td>);
      }
      // render khoảng trống
      if (index == 6) {
        contentHTML.push(<td></td>);
      }
      // render mấy cái ghế trong hàng
      if (customerInfor.isSelect === true) {
        if (Number(gheDangChon.length) < Number(customerInfor.soLuongGhe)) {
          //giới hạn số lựa ghế khách hàng đặt
          contentHTML.push(
            // daDat có giá trị true khi đã được đặt, lúc này isDisable = daDAt = true hoặc ngược lại
            this.renderSeat(soGhe, daDat, daDat, this.handleSelectSeats)
          );
        } else {
          // trường hợp số lượng ghế đang chọn đã bằng số lượng ghế khách đặt
          if (viTriGheDaChon === -1) {
            contentHTML.push(
              this.renderSeat(soGhe, daDat, true, this.handleSelectSeats)
            );
          } else {
            contentHTML.push(
              this.renderSeat(soGhe, daDat, false, this.handleSelectSeats)
            );
          }
        }
      } else {
        // render ghế khi chưa select mục đích k cho user nhấn vào
        contentHTML.push(
          this.renderSeat(soGhe, daDat, true, this.handleSelectSeats)
        );
      }
    }
    return <tr>{contentHTML}</tr>;
  };
  renderAllSeats = (dataSeats, customerInfor, seatsOccupied) => {
    let seats = [];
    for (let index = 0; index < dataSeats.length; index++) {
      let currentRow = dataSeats[index];
      if (index == 0) {
        seats.push(this.renderNumberRow(currentRow));
      } else {
        seats.push(this.renderRow(currentRow, customerInfor, seatsOccupied));
      }
    }
    return seats;
  };
  render() {
    return (
      <div className="text-center">
        <table>
          <tbody>
            {this.renderAllSeats(
              this.props.seatsAvailable,
              this.props.customerInfor,
              this.props.seatsOccupied
            )}
          </tbody>
        </table>
        <button
          onClick={() => {
            this.setState({ gheDangChon: [] });
            this.props.handleComfirm(this.state.gheDangChon);
          }}
          class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow my-6"
        >
          Comfirm
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    seatsAvailable: state.DatVeReducer.seatsAvailable,
    customerInfor: state.DatVeReducer.customerInfor,
    seatsOccupied: state.DatVeReducer.seatsOccupied,
  };
};
let mapDispathToProps = (dispath) => {
  return {
    handleComfirm: (gheDangChon) => {
      dispath(guiGheDaChon(gheDangChon));
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(Selection);
