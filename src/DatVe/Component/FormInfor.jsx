import React, { Component } from "react";
import { connect } from "react-redux";
import { sentUserInfor, xacNhanThongTin } from "../../redux/action/action";

class FormInfor extends Component {
  state = {
    value: {
      ten: "",
      soLuongGhe: 0,
    },
    error: {
      ten: "",
      soLuongGhe: "",
    },
  };
  handleChange = (e) => {
    let inputValue = e.target;
    let { name, value } = inputValue;
    let errorMessage = "";
    if (value === "") {
      errorMessage = "Trường này không được để rỗng";
    }
    if (value == 0) {
      errorMessage = "Trường này phải khác 0";
    }
    let values = { ...this.state.value, [name]: value };
    let errors = { ...this.state.error, [name]: errorMessage };
    this.setState(
      {
        value: values,
        error: errors,
      },
      () => {
        this.props.handleSent(this.state.value);
      }
    );
  };
  handleSelect = () => {
    let { ten, soLuongGhe } = this.state.error;
    if (this.state.value.ten === "" || this.state.value.soLuongGhe == 0) {
      return;
    }
    if (ten === "" && soLuongGhe === "") {
      this.props.handleXacNhanThongTin();
    }
  };
  render() {
    return (
      <form className="w-full max-w-sm">
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label
              className="block text-white font-bold md:text-right mb-1 md:mb-0 pr-4"
              htmlFor="inline-full-name"
            >
              Name
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              id="inline-full-name"
              type="text"
              name="ten"
              onChange={this.handleChange}
              value={this.props.customerInfor.ten}
            />
            <p className="text-red-700 text-xs">{this.state.error.ten}</p>
          </div>
        </div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label
              className="block text-white font-bold md:text-right mb-1 md:mb-0 pr-4"
              htmlFor="inline-password"
            >
              Numbers of seat
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              id="inline-password"
              type="number"
              name="soLuongGhe"
              onChange={this.handleChange}
              value={this.props.customerInfor.soLuongGhe}
            />
            <p className="text-red-700 text-xs">
              {this.state.error.soLuongGhe}
            </p>
          </div>
        </div>
        <div className="md:flex md:items-center">
          <div className="md:w-1/3" />
          <div className="md:w-2/3">
            <button
              onClick={this.handleSelect}
              className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
              type="button"
            >
              Select
            </button>
          </div>
        </div>
      </form>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    customerInfor: state.DatVeReducer.customerInfor,
  };
};
let mapDispathToProps = (dispath) => {
  return {
    handleSent: (payload) => {
      dispath(sentUserInfor(payload));
    },
    handleXacNhanThongTin: () => {
      dispath(xacNhanThongTin());
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(FormInfor);
