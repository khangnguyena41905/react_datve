import React, { Component } from "react";
import { connect } from "react-redux";

class Table extends Component {
  handleTable = (tableList) => {
    return tableList.map((user, index) => {
      console.log("user: ", user);
      return (
        <tr key={index} class="bg-white border-b  ">
          <td class="py-4 px-6 text-gray-700 ">{user.ten}</td>
          <td class="py-4 px-6 text-gray-500">{user.soLuongGhe}</td>
          <td class="py-4 px-6 text-gray-500">
            {user.gheDaChon.map((ghe) => {
              return <span>{ghe},</span>;
            })}
          </td>
          <td class="py-4 px-6 text-gray-500">$2999</td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="text-center">
        {/* table */}

        <div class="overflow-x-auto relative">
          <table class="w-full text-sm text-left text-gray-500 ">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 ">
              <tr className="text-center">
                <th scope="col" class="py-3 px-6">
                  Name
                </th>
                <th scope="col" class="py-3 px-6">
                  Number of seat
                </th>
                <th scope="col" class="py-3 px-6">
                  Seats
                </th>
                <th scope="col" class="py-3 px-6">
                  Price
                </th>
              </tr>
            </thead>
            <tbody>{this.handleTable(this.props.tableList)}</tbody>
          </table>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    tableList: state.DatVeReducer.tableList,
  };
};

export default connect(mapStateToProps)(Table);
