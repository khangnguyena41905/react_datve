import logo from "./logo.svg";
import "./App.css";
import Ex_DatVe from "./DatVe/Ex_DatVe";

function App() {
  return (
    <div className="App">
      <Ex_DatVe />
    </div>
  );
}

export default App;
