import { combineReducers } from "redux";
import * as reducers from "./reducer";

export let rootReducer = combineReducers({ ...reducers });
