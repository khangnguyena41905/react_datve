import { data } from "../../asset/source/danhSachGhe";
import {
  GUI_GHE_DA_CHON,
  SENT_USER_INFOR,
  XAC_NHAN_THONG_TIN,
} from "../contant/contant";
const initialState = {
  customerInfor: {
    ten: "",
    soLuongGhe: 0,
    gheDaChon: [],
    isSelect: false,
  },
  seatsOccupied: [],
  seatsAvailable: [...data],
  tableList: [],
};

export const DatVeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SENT_USER_INFOR: {
      let newCustomer = {
        ...state.customerInfor,
        ten: payload.ten,
        soLuongGhe: payload.soLuongGhe,
      };
      state.customerInfor = newCustomer;
      return { ...state };
    }
    case XAC_NHAN_THONG_TIN: {
      let currentCustomer = { ...state.customerInfor, isSelect: true };
      state.customerInfor = currentCustomer;
      console.log(state.customerInfor);
      return { ...state };
    }
    case GUI_GHE_DA_CHON: {
      state.seatsOccupied.push(payload);
      let dataSeats = [...state.seatsAvailable];
      for (let index = 0; index < payload.length; index++) {
        let ghe = payload[index];
        let indexHang = dataSeats.findIndex((e) => {
          return e.hang == ghe[0];
        });
        if (indexHang !== -1) {
          let indexGhe = dataSeats[indexHang].danhSachGhe.findIndex((e) => {
            return e.soGhe == ghe;
          });
          if (indexGhe !== -1) {
            dataSeats[indexHang].danhSachGhe[indexGhe].daDat = true;
          }
        }
      }
      state.seatsAvailable = dataSeats;
      let currentCustomerInfor = { ...state.customerInfor, gheDaChon: payload };
      state.tableList = [...state.tableList, currentCustomerInfor];
      let resetCustomerInfor = {
        ten: "",
        soLuongGhe: 0,
        gheDaChon: [],
        isSelect: false,
      };
      state.customerInfor = resetCustomerInfor;
      return { ...state };
    }
    default:
      return state;
  }
};
