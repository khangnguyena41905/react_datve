import {
  GUI_GHE_DA_CHON,
  LUA_CHON_GHE,
  SENT_USER_INFOR,
  XAC_NHAN_THONG_TIN,
} from "../contant/contant";

export const sentUserInfor = (payload) => ({
  type: SENT_USER_INFOR,
  payload,
});
export const guiGheDaChon = (gheDangChon) => ({
  type: GUI_GHE_DA_CHON,
  payload: gheDangChon,
});
export const xacNhanThongTin = () => ({
  type: XAC_NHAN_THONG_TIN,
});
