/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/App.js",
    "./src/DatVe/Ex_DatVe.jsx",
    "./src/DatVe/Component/FormInfor.jsx",
    "./src/DatVe/Component/Selection.jsx",
    "./src/DatVe/Component/Table.jsx",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
